# PLN.Infra.ParReader
A library to read some parts of par2 files. Specifically to read the file information.

    var p2 = Par2.ReadFile("myparfile.par2");
    Console.WriteLine("Creator: {0}", p2.Creator.CreatorName);
    foreach (var desc in p2.FileDescs)
    {
        Console.WriteLine("File: {0}", desc.FileName);
        Console.WriteLine("Size: {0}", desc.FileLength);
        Console.WriteLine("MD5 : {0}", BitConverter.ToString(desc.FileHash));
    }
