﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;
using System.Text;
using System.Text.RegularExpressions;
using PLN.Infra.ParReader.Par2Packet;

namespace PLN.Infra.ParReader
{

	// Based on http://parchive.sourceforge.net/docs/specifications/parity-volume-spec/article-spec.html

	public class Par2
	{

		internal static readonly Regex ParVolume = new Regex(@"(.+)\.vol[0-9]{1,10}\+[0-9]{1,10}\.par2$", RegexOptions.IgnoreCase);

		public const string Par2PacketHeaderMagic = "PAR2\0PKT";

		public string ParName { get; set; }
		public string FileName { get; set; }
		public string FilePath { get; set; }

		public bool IsMain { get; set; }
		public bool IsVolume { get; set; }

		public Main Main { get; set; }
		public Creator Creator { get; set; }
		public List<FileDesc> FileDescs { get; set; }

		public string Index { get; set; }
		public List<string> Volumes { get; set; }
		public List<string> VolumeSpares { get; set; }
		public List<string> FileSpares { get; set; }

		public ulong SizeParityFiles { get; set; }
		public ulong SizeParredFiles { get; set; }
		public ulong SizeCombined { get; set; }

		public static Par2 ReadFile(string path)
		{
			// Expand the path with the Current Directory if it is not a full path.
			if (!Path.IsPathRooted(path))
				path = Path.Combine(Environment.CurrentDirectory, path);

			var directory = Path.GetDirectoryName(path);

			var result = new Par2
			{
				FileName = Path.GetFileName(path),
				FilePath = path,
				FileDescs = new List<FileDesc>(),
				Volumes = new List<string>(),
				VolumeSpares = new List<string>(),
				FileSpares = new List<string>(),
			};

			// First attempt to parse the par2 file. This gives us a base of information to work with.
			using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				while (stream.Position < stream.Length)
				{
					// Read the next packet.
					var packet = ReadPacket(stream);

					// Keep track of some of these packets.
					if (packet is Main)
						result.Main = packet as Main;
					if (packet is Creator)
						result.Creator = packet as Creator;
					if (packet is FileDesc)
					{
						var newFile = packet as FileDesc;
						if (!result.FileDescs.Any(fd => fd.FileName.Equals(newFile.FileName)))
							result.FileDescs.Add(newFile);
					}

					// Other packets are ignored for now.
				}
			}


			// Check if the given path matches our volume regex.
			var volMatch = ParVolume.Match(result.FileName);
			result.IsMain = !(result.IsVolume = volMatch.Success);

			// Determine the ParName.
			result.ParName = result.IsMain ? Path.GetFileNameWithoutExtension(result.FileName) : volMatch.Groups[1].Value;

			// Determine the Index, if it exists.
			result.Index = String.Concat(result.ParName, ".par2");
			if (!File.Exists(Path.Combine(directory, result.Index)))
				result.Index = null;

			// Grab the files present in the par2's directory.
			var fileNames = Directory.GetFiles(directory).Select(Path.GetFileName).ToArray();

			// Remove files from the fileNames.
			fileNames = fileNames.Where(fn => result.FileDescs.All(v => String.Compare(fn, v.FileName, StringComparison.InvariantCultureIgnoreCase) != 0)).ToArray();

			// Look for all volumes.
			result.Volumes = fileNames.Select(fn => new { Name = fn, Match = ParVolume.Match(fn) }).Where(m => m.Match.Success)
				.Select(m => new { m.Name, ParName = m.Match.Groups[1].Value })
				.Where(m => result.ParName.Equals(m.ParName, StringComparison.InvariantCultureIgnoreCase))
				.Select(m => m.Name).ToList();

			// Remove self and volumes from the fileNames.
			fileNames = fileNames.Where(fn => String.Compare(fn, result.FileName, StringComparison.InvariantCultureIgnoreCase) != 0).ToArray();
			fileNames = fileNames.Where(fn => result.Volumes.All(v => String.Compare(fn, v, StringComparison.InvariantCultureIgnoreCase) != 0)).ToArray();

			// Look for Volume Spares.
			result.VolumeSpares = fileNames.Where(fn => result.Volumes.Any(v => fn.StartsWith(v, StringComparison.InvariantCultureIgnoreCase))).ToList();
			fileNames = fileNames.Where(fn => result.VolumeSpares.All(v => String.Compare(fn, v, StringComparison.InvariantCultureIgnoreCase) != 0)).ToArray();

			// Look for File Spares.
			result.FileSpares = fileNames.Where(fn => result.FileDescs.Any(v => fn.StartsWith(v.FileName, StringComparison.InvariantCultureIgnoreCase))).ToList();

			// Sum the Parity File sizes.
			result.SizeParityFiles = result.Volumes.Sum(v => v.GetFileSize(directory));
			if (!String.IsNullOrEmpty(result.Index))
				result.SizeParityFiles += result.Index.GetFileSize(directory);

			// Sum the Parred File sizes.
			result.SizeParredFiles = result.FileDescs.Sum(fd => fd.FileLength);

			// And combine the two.
			result.SizeCombined = result.SizeParityFiles + result.SizeParredFiles;

			return result;
		}

		public static IPar2Packet ReadPacket(Stream stream)
		{
			// Read a Packet Header.
			var header = stream.ReadStruct<Par2PacketHeader>();

			// Test if the magic constant matches.
			var magic = Encoding.ASCII.GetString(header.Magic);
			if (!Par2PacketHeaderMagic.Equals(magic))
				throw new ApplicationException("Invalid Magic Constant");

			// Determine which type of packet we have.
			var packetType = Encoding.ASCII.GetString(header.PacketType);
			IPar2Packet result;
			switch (packetType)
			{
				case FileDesc.PacketType:
					result = new FileDesc(header);
					break;
				case InputFileSliceChecksum.PacketType:
					result = new InputFileSliceChecksum(header);
					break;
				case Main.PacketType:
					result = new Main(header);
					break;
				case Creator.PacketType:
					result = new Creator(header);
					break;
				case RecoverySlice.PacketType:
					result = new RecoverySlice(header);
					break;
				default:
					throw new ApplicationException(String.Concat("Unknown Packet Type encountered: ", packetType));
			}

			// Let the packet type parse more of the stream as needed.
			result.Read(stream);

			return result;
		}

	}

}