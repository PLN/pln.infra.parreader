﻿using System;
using System.Linq;

namespace PLN.Infra.ParReader
{
	public static class Version
	{

		private const float RevisionFactor = (float)(UInt16.MaxValue - 1) / (24 * 60 * 60);

		public static readonly ushort Major = 0;
		public static readonly ushort Minor = 2;
		public static readonly ushort Build = 0;
		public static readonly ushort Revision = 0;

		public static string ShortText
		{
			get { return String.Format("v{0}.{1}", Major, Minor); }
		}

		public static string LongText
		{
			get { return String.Format("v{0}.{1}.{2}.{3}", Major, Minor, Build, Revision); }
		}

		public static DateTime Date
		{
			get { return new DateTime(1969, 12, 31).AddDays(Build).AddSeconds(Revision / RevisionFactor); }
		}

		public static string DateText
		{
			get { return String.Format("v{0}.{1}.{2}.{3} ({4})", Major, Minor, Build, Revision, Date.ToString("yyyy-MM-dd HH:mm:ss")); }
		}

		public static string ShortDateText
		{
			get { return String.Format("v{0}.{1} ({2})", Major, Minor, Date.ToString("yyyy-MM-dd HH:mm:ss")); }
		}

	}
}
