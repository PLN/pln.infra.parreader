﻿using System.Linq;
using System;

namespace PLN.Infra.ParReader.Par2Packet
{

	public class Main : Par2PacketBase
	{

		public const string PacketType = "PAR 2.0\0Main\0\0\0\0";

		public Main(Par2PacketHeader header)
			: base(header)
		{ }

		protected override void ParseBody(byte[] body)
		{ }

	}

}