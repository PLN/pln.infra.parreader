﻿using System.IO;
using System.Linq;
using System;
using System.Runtime.InteropServices;

namespace PLN.Infra.ParReader.Par2Packet
{

	/// <summary>
	/// Implements the basic Read mechanism, passing the body bytes to any child class.
	/// </summary>
	public abstract class Par2PacketBase : IPar2Packet
	{

		public Par2PacketHeader Header { get; protected set; }

		protected Par2PacketBase(Par2PacketHeader header)
		{
			Header = header;
		}

		public void Read(Stream stream)
		{
			// Determine the length of the body as the given packet length, minus the length of the header.
			var bodyLength = Header.PacketLength - (ulong)Marshal.SizeOf(typeof (Par2PacketHeader));

			// Read the calculated number of bytes from the stream.
			var body = new byte[bodyLength];
			stream.Read(body, 0, (int)bodyLength);

			// Pass the body to the further implementation for parsing.
			ParseBody(body);
		}

		protected abstract void ParseBody(byte[] body);

	}

}