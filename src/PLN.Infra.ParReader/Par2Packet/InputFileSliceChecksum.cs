﻿using System.Linq;
using System;

namespace PLN.Infra.ParReader.Par2Packet
{

	public class InputFileSliceChecksum : Par2PacketBase
	{

		public const string PacketType = "PAR 2.0\0IFSC\0\0\0\0";

		public InputFileSliceChecksum(Par2PacketHeader header) : base(header)
		{}

		protected override void ParseBody(byte[] body)
		{}

	}

}