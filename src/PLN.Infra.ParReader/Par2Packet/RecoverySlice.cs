﻿using System.Linq;
using System;

namespace PLN.Infra.ParReader.Par2Packet
{

	public class RecoverySlice : Par2PacketBase
	{

		public const string PacketType = "PAR 2.0\0RecvSlic";

		public RecoverySlice(Par2PacketHeader header)
			: base(header)
		{ }

		protected override void ParseBody(byte[] body)
		{ }

	}

}