﻿using System.IO;
using System.Linq;
using System;

namespace PLN.Infra.ParReader.Par2Packet
{

	public interface IPar2Packet
	{

		Par2PacketHeader Header { get; }
		void Read(Stream stream);

	}

}