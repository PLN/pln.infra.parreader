﻿using System.Linq;
using System;
using System.Text;

namespace PLN.Infra.ParReader.Par2Packet
{

	public class FileDesc : Par2PacketBase
	{

		public const string PacketType = "PAR 2.0\0FileDesc";

		public byte[] FileID { get; protected set; }
		public byte[] FileHash { get; protected set; }
		public byte[] File16kHash { get; protected set; }
		public ulong FileLength { get; protected set; }
		public string FileName { get; protected set; }

		public FileDesc(Par2PacketHeader header) : base(header)
		{}

		protected override void ParseBody(byte[] body)
		{
			// 16	MD5 Hash	The File ID.
			FileID = new byte[16];
			Buffer.BlockCopy(body, 0, FileID, 0, 16);

			// 16	MD5 Hash	The MD5 hash of the entire file.
			FileHash = new byte[16];
			Buffer.BlockCopy(body, 16, FileHash, 0, 16);

			// 16	MD5 Hash	The MD5-16k. That is, the MD5 hash of the first 16kB of the file.
			File16kHash = new byte[16];
			Buffer.BlockCopy(body, 32, File16kHash, 0, 16);

			// 8	8-byte uint	Length of the file.
			FileLength = BitConverter.ToUInt64(body, 48);

			// ?*4	ASCII char array	Name of the file. This array is not guaranteed to be null terminated! Subdirectories are indicated by an HTML-style '/' (a.k.a. the UNIX slash). The filename must be unique.
			var nameBuffer = new byte[body.Length - 56];
			Buffer.BlockCopy(body, 56, nameBuffer, 0, nameBuffer.Length);

			// Use UTF8 encoding if it is either ASCII or has valid byte sequences. Otherwise, go with Windows-1252.
			var encoding = nameBuffer.IsUTF8() ? Encoding.UTF8 : Encoding.GetEncoding(1252);
			FileName = encoding.GetString(nameBuffer).Normalize().TrimZero();
		}

		public override string ToString()
		{
			return FileName ?? "FileDesc";
		}

	}

}