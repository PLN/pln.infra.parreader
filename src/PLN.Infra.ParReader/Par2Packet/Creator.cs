﻿using System.Linq;
using System;

namespace PLN.Infra.ParReader.Par2Packet
{

	public class Creator : Par2PacketBase
	{

		public const string PacketType = "PAR 2.0\0Creator\0";

		public string CreatorName { get; protected set; }

		public Creator(Par2PacketHeader header)
			: base(header)
		{ }

		protected override void ParseBody(byte[] body)
		{
			// ?*4	ASCII char array	ASCII text identifying the client. This should also include a way to contact the client's creator - either through a URL or an email address. NB: This is not a null terminated string!
			CreatorName = System.Text.Encoding.ASCII.GetString(body).TrimZero();
		}

		public override string ToString()
		{
			return CreatorName ?? "Creator";
		}

	}

}