﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;
using System.Runtime.InteropServices;

namespace PLN.Infra.ParReader
{

	public static class Extensions
	{

		/// <summary>
		/// Read a struct as binary from a stream.
		/// </summary>
		/// <typeparam name="T">The struct to read.</typeparam>
		/// <param name="stream">The stream to read from.</param>
		/// <returns>The struct with values read from the stream.</returns>
		public static T ReadStruct<T>(this Stream stream) where T : struct
		{
			var sz = Marshal.SizeOf(typeof(T));
			var buffer = new byte[sz];
			stream.Read(buffer, 0, sz);
			var pinnedBuffer = GCHandle.Alloc(buffer, GCHandleType.Pinned);
			var structure = (T)Marshal.PtrToStructure(pinnedBuffer.AddrOfPinnedObject(), typeof(T));
			pinnedBuffer.Free();
			return structure;
		}

		/// <summary>
		/// Strips any ending \0's from the input string.
		/// </summary>
		public static string TrimZero(this string input)
		{
			while (input.EndsWith("\0"))
				input = input.Substring(0, input.Length - 1);
			return input;
		}

		public static ulong GetFileSize(this string name, string directory)
		{
			var vi = new FileInfo(Path.Combine(directory, name));
			if (!vi.Exists)
				return 0;
			return (ulong)vi.Length;
		}

		public static ulong Sum(this IEnumerable<ulong> source)
		{
			return source.Aggregate(0UL, (c, v) => c + v);
		}

		public static ulong? Sum(this IEnumerable<ulong?> source)
		{
			return source.Aggregate(0UL, (c, v) => c + v.GetValueOrDefault());
		}

		public static ulong Sum<T>(this IEnumerable<T> source, Func<T, ulong> selector)
		{
			return source.Select(selector).Sum();
		}

		public static ulong? Sum<T>(this IEnumerable<T> source, Func<T, ulong?> selector)
		{
			return source.Select(selector).Sum();
		}

		public static bool IsUTF8(this byte[] input)
		{
			if (input == null)
				return false;
			if (input.Length == 0)
				return false;

			// TODO: Check for a BOM.

			for (int i = 0; i < input.Length; i++)
			{
				// Skip low bytes.
				if (input[i] < 0x80)
					continue;

				// Start of 4-byte sequence
				if ((input[i] & 0xF8) == 0xF0)
				{
					if (i + 3 >= input.Length)
						return false; // Invalid sequence length.
					if ((input[i + 1] & 0xC0) != 0x80)
						return false; // Invalid sequence.
					if ((input[i + 2] & 0xC0) != 0x80)
						return false; // Invalid sequence.
					if ((input[i + 3] & 0xC0) != 0x80)
						return false; // Invalid sequence.
					i += 3; // Valid sequence. Skip to the next character.
					continue;
				}

				// Start of 3-byte sequence
				if ((input[i] & 0xF0) == 0xE0)
				{
					if (i + 2 >= input.Length)
						return false; // Invalid sequence length.
					if ((input[i + 1] & 0xC0) != 0x80)
						return false; // Invalid sequence.
					if ((input[i + 2] & 0xC0) != 0x80)
						return false; // Invalid sequence.
					i += 2; // Valid sequence. Skip to the next character.
					continue;
				}

				// Start of 2-byte sequence
				if ((input[i] & 0xE0) == 0xC0)
				{
					if (i + 1 >= input.Length)
						return false; // Invalid sequence length.
					if ((input[i + 1] & 0xC0) != 0x80)
						return false; // Invalid sequence.
					i += 1; // Valid sequence. Skip to the next character.
					continue;
				}
			}

			// Either all high bytes are valid sequences, or there are no high bytes, and US-ASCII is valid UTF-8.
			return true;

			// From http://stackoverflow.com/questions/11479143/detect-utf-8-encoding-how-does-ms-ide-do-it
//			unc ::IsUTF8(unc *cpt)
//			{
//				if (!cpt)
//					return 0;
//
//				if ((*cpt & 0xF8) == 0xF0)
//				{ // start of 4-byte sequence
//					if (((*(cpt + 1) & 0xC0) == 0x80)
//					 && ((*(cpt + 2) & 0xC0) == 0x80)
//					 && ((*(cpt + 3) & 0xC0) == 0x80))
//						return 4;
//				}
//				else if ((*cpt & 0xF0) == 0xE0)
//				{ // start of 3-byte sequence
//					if (((*(cpt + 1) & 0xC0) == 0x80)
//					 && ((*(cpt + 2) & 0xC0) == 0x80))
//						return 3;
//				}
//				else if ((*cpt & 0xE0) == 0xC0)
//				{ // start of 2-byte sequence
//					if ((*(cpt + 1) & 0xC0) == 0x80)
//						return 2;
//				}
//				return 0;
//			}
		}

	}

}