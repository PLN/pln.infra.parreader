@echo off

PushD %~dp0

SET VCVARS=vcvarsall.bat
IF EXIST "%ProgramFiles(x86)%\Microsoft Visual Studio 11.0\Common7\Tools\VsDevCmd.bat" SET VCVARS=%ProgramFiles(x86)%\Microsoft Visual Studio 11.0\Common7\Tools\VsDevCmd.bat
IF EXIST "%ProgramFiles(x86)%\Microsoft Visual Studio 10.0\VC\vcvarsall.bat" SET VCVARS=%ProgramFiles(x86)%\Microsoft Visual Studio 10.0\VC\vcvarsall.bat
IF EXIST "%ProgramFiles(x86)%\Microsoft Visual Studio 11.0\VC\vcvarsall.bat" SET VCVARS=%ProgramFiles(x86)%\Microsoft Visual Studio 11.0\VC\vcvarsall.bat
IF EXIST "%ProgramFiles(x86)%\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" SET VCVARS=%ProgramFiles(x86)%\Microsoft Visual Studio 12.0\VC\vcvarsall.bat
Call "%VCVARS%"

msbuild PLN.Infra.ParReader.sln /t:Build /p:Configuration=Release

PushD src\PLN.Infra.ParReader\bin\NuGet\
FOR %%F IN (*.nupkg) DO (
 Set filename=%%F
 Goto PushPackage
)
:PushPackage
echo About to push "%filename%"
pause
..\..\..\..\tools\NuGet\NuGet.exe Push "%filename%"
PopD

PopD
pause
