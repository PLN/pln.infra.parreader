﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PLN.Infra.ParReader.Test
{
	[TestClass]
	public class Par2Test
	{

		[TestMethod]
		public void Case1()
		{

			var tp2 = Par2.ReadFile(Solution.Case1Par2);
			Assert.IsNotNull(tp2);
			Assert.IsNotNull(tp2.Main);
			Assert.IsNotNull(tp2.Creator);
			Assert.AreEqual(3, tp2.FileDescs.Count);

			var license = tp2.FileDescs.FirstOrDefault(fd => fd.FileName.Equals("license.txt"));
			Assert.IsNotNull(license);
			Assert.AreEqual((UInt64)1109, license.FileLength);
			Assert.AreEqual("license.txt", license.FileName);

		}

		[TestMethod]
		public void Case2()
		{
			var p2 = Par2.ReadFile(Solution.Case2Par2);
			Assert.IsNotNull(p2);
			Assert.AreEqual("files.par2", p2.FileName);
			Assert.AreEqual("files", p2.ParName);
			Assert.AreEqual(true, p2.IsMain);
			Assert.AreEqual(false, p2.IsVolume);

			Assert.AreEqual(7, p2.Volumes.Count);
			Assert.IsTrue(p2.Volumes.Contains("files.vol00+01.par2"));
			Assert.IsTrue(p2.Volumes.Contains("files.vol63+36.par2"));

			Assert.AreEqual(1, p2.VolumeSpares.Count);
			Assert.IsTrue(p2.VolumeSpares.Contains("files.vol01+02.par2.1"));

			Assert.AreEqual(2, p2.FileSpares.Count);
			Assert.IsTrue(p2.FileSpares.Contains("File_3.txt.2"));


			var p2v = Par2.ReadFile(Solution.Case2Par2Vol);
			Assert.IsNotNull(p2v);
			Assert.AreEqual("files.vol03+04.par2", p2v.FileName);
			Assert.AreEqual("files", p2v.ParName);
			Assert.AreEqual(false, p2v.IsMain);
			Assert.AreEqual(true, p2v.IsVolume);

			Assert.AreEqual(7, p2v.Volumes.Count);
			Assert.IsTrue(p2v.Volumes.Contains("files.vol00+01.par2"));
			Assert.IsTrue(p2v.Volumes.Contains("files.vol63+36.par2"));

			Assert.AreEqual(1, p2v.VolumeSpares.Count);
			Assert.IsTrue(p2v.VolumeSpares.Contains("files.vol01+02.par2.1"));

			Assert.AreEqual(2, p2v.FileSpares.Count);
			Assert.IsTrue(p2v.FileSpares.Contains("File_3.txt.2"));
		}

		[TestMethod]
		public void Case2ParsingVolumesReturnsAUniqueFileList()
		{
			var p2 = Par2.ReadFile(Solution.Case2Par2Vol);
			Assert.AreEqual(5, p2.FileDescs.Count);
		}

		[TestMethod]
		public void Case2ParsingVolumeFindsTheIndex()
		{
			var p2 = Par2.ReadFile(Solution.Case2Par2Vol);
			Assert.AreEqual("files.par2", p2.Index);
		}

		[TestMethod]
		public void Case2SizeValidation()
		{
			var p2 = Par2.ReadFile(Solution.Case2Par2);
			Assert.AreEqual(1179052UL, p2.SizeParityFiles);
			Assert.AreEqual(507650UL, p2.SizeParredFiles);
			Assert.AreEqual(1179052UL + 507650UL, p2.SizeCombined);
		}


		[TestMethod]
		public void Case3()
		{
			var p2 = Par2.ReadFile(Solution.Case3Par2);
			Assert.IsNotNull(p2);
			Assert.AreEqual("file.txt.par2", p2.FileName);
			Assert.AreEqual("file.txt", p2.ParName);
			Assert.AreEqual(true, p2.IsMain);
			Assert.AreEqual(false, p2.IsVolume);

			Assert.AreEqual(7, p2.Volumes.Count);
			Assert.IsTrue(p2.Volumes.Contains("file.txt.vol000+01.par2"));
			Assert.IsTrue(p2.Volumes.Contains("file.txt.vol063+37.par2"));

			Assert.AreEqual(0, p2.VolumeSpares.Count);

			Assert.AreEqual(1, p2.FileSpares.Count);
			Assert.IsTrue(p2.FileSpares.Contains("file.txt.1"));
		}

		[TestMethod]
		public void Case4()
		{
			var p2 = Par2.ReadFile(Solution.Case4Par2);
			Assert.IsNotNull(p2);
			Assert.AreEqual("tëst.par2", p2.FileName);
			Assert.AreEqual(1, p2.FileDescs.Count);
			Assert.AreEqual("tëst.txt", p2.FileDescs[0].FileName);
		}

	}

}
