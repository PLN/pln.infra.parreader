﻿using System;
using System.IO;
using System.Linq;

namespace PLN.Infra.ParReader.Test
{
	public static class Solution
	{

		public static readonly string Directory = @""; // Directory

		public static readonly string Case1Par2 = Path.Combine(Directory, @"test\Case1\test.par2");
		public static readonly string Case2Par2 = Path.Combine(Directory, @"test\Case2\files.par2");
		public static readonly string Case2Par2Vol = Path.Combine(Directory, @"test\Case2\files.vol03+04.par2");
		public static readonly string Case3Par2 = Path.Combine(Directory, @"test\Case3\file.txt.par2");
		public static readonly string Case4Par2 = Path.Combine(Directory, @"test\Case4\tëst.par2");

	}
}
